package com.ardanu.challenge_ch4

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LayoutDataStore(private val context: Context) {



    suspend fun setLayoutPref(isGrid: Boolean) {
        context.layoutDataStore.edit { preferences ->
            preferences[LAYOUT_KEY] = isGrid
        }
    }

    fun getLayoutPref(): Flow<Boolean> = context.layoutDataStore.data.map { preferences ->
        preferences[LAYOUT_KEY] ?: true
    }

    companion object{
        private val Context.layoutDataStore: DataStore<Preferences> by preferencesDataStore(
            name = "layout_preference"
        )
        private val LAYOUT_KEY = booleanPreferencesKey("isGrid_key")
    }
}