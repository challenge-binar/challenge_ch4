package com.ardanu.challenge_ch4.fragments

import android.app.Dialog
import android.content.res.ColorStateList
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ardanu.challenge_ch4.R
import com.ardanu.challenge_ch4.ViewModelFactory
import com.ardanu.challenge_ch4.adapter.CartAdapter
import com.ardanu.challenge_ch4.databinding.FragmentOrderBinding
import com.ardanu.challenge_ch4.databinding.SuccessOrderDialogBinding
import com.ardanu.challenge_ch4.viewmodel.CartViewModel

@Suppress("DEPRECATION")
class OrderFragment : Fragment() {
    private lateinit var binding: FragmentOrderBinding
    private lateinit var cartViewModel: CartViewModel
    private lateinit var dialogBinding: SuccessOrderDialogBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentOrderBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewModel()
        chooseDelivery()
        showRecyclerVIew()
        setEventUI()
        choosePayment()
        setRingkasan()
    }

    private fun setRingkasan(){
        cartViewModel.allCarts.observe(viewLifecycleOwner){
            var listMenu = ""
            var priceMenu = ""
            var totalPrice = 0
            it.forEach {item ->
                listMenu += "${item.name} - ${item.quantity} x ${item.basePrice}\n"
                priceMenu += "Rp. ${item.totalPrice}\n"
                totalPrice += item.totalPrice
            }

            val totalText = "Rp. $totalPrice"
            binding.txtNamaMenu.text = listMenu
            binding.txtTotalMenu.text = priceMenu
            binding.txtTotalHarga.text = totalText
        }
    }
    private fun setupViewModel(){
        val factory = ViewModelFactory(requireActivity().application)
        cartViewModel = ViewModelProvider(this,factory)[CartViewModel::class.java]
    }

    private fun showRecyclerVIew(){
        val adapter = CartAdapter(cartViewModel,isOrder = true)

        binding.rvOrder.adapter = adapter
        binding.rvOrder.layoutManager = LinearLayoutManager(requireContext())

        cartViewModel.allCarts.observe(viewLifecycleOwner){
            adapter.setList(it)

            var totalPrice = 0
            it.forEach {item ->
                totalPrice += item.totalPrice
            }
        }
    }

    private fun setEventUI(){
        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.btnPesan.setOnClickListener {
            dialogBinding = SuccessOrderDialogBinding.inflate(layoutInflater)

            val dialogSuccess = Dialog(requireContext())
            dialogSuccess.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialogSuccess.setContentView(dialogBinding.root)
            dialogSuccess.setCancelable(true)
            dialogSuccess.setCanceledOnTouchOutside(false)
            dialogSuccess.show()
            dialogBinding.btnBackHome.setOnClickListener {
                cartViewModel.deleteAllCart()
                findNavController().navigate(R.id.homeFragment)
                dialogSuccess.hide()
            }
        }
    }
    private fun chooseDelivery(){
        binding.btnDelivery.setOnClickListener {
            binding.btnTakeaway.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.grey))
            binding.btnDelivery.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.light_primary))
        }

        binding.btnTakeaway.setOnClickListener {
            binding.btnTakeaway.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.light_primary))
            binding.btnDelivery.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.grey))
        }
    }
    private fun choosePayment(){
        binding.btnTunai.setOnClickListener {
            binding.btnDompet.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.grey))
            binding.btnTunai.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.light_primary))
        }

        binding.btnDompet.setOnClickListener {
            binding.btnDompet.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.light_primary))
            binding.btnTunai.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.grey))
        }
    }
}