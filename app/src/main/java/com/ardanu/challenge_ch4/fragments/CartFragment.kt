package com.ardanu.challenge_ch4.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ardanu.challenge_ch4.R
import com.ardanu.challenge_ch4.ViewModelFactory
import com.ardanu.challenge_ch4.adapter.CartAdapter
import com.ardanu.challenge_ch4.databinding.FragmentCartBinding
import com.ardanu.challenge_ch4.viewmodel.CartViewModel

class CartFragment : Fragment() {
    private lateinit var binding: FragmentCartBinding
    private lateinit var cartViewModel: CartViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewModel()
        showCartItems()
        actionToPesan()
    }

    private fun setupViewModel() {
        val factory = ViewModelFactory(requireActivity().application)
        cartViewModel = ViewModelProvider(this, factory)[CartViewModel::class.java]
    }

    private fun showCartItems() {
        val adapter = CartAdapter(cartViewModel, isOrder = false)
        binding.rvCart.adapter = adapter
        binding.rvCart.layoutManager = LinearLayoutManager(requireContext())

        cartViewModel.allCarts.observe(viewLifecycleOwner) {

            if (it.isEmpty()) {
                binding.rvCart.isVisible = false
                binding.layoutNoData.root.isVisible = true
                binding.btnPesan.isEnabled = false
            } else {
                binding.layoutNoData.root.isVisible = false
                binding.rvCart.isVisible = true
                adapter.setList(it)

                var totalPrice = 0
                it.forEach { item ->
                    totalPrice += item.totalPrice
                }
                val priceText = "Rp. $totalPrice"
                binding.txtTotalPrice.text = priceText
            }
        }
    }

    private fun actionToPesan() {
        binding.btnPesan.setOnClickListener {
            val navOptions = NavOptions.Builder()
                .setPopUpTo(R.id.cartFragment, false)
                .build()
            findNavController().navigate(
                R.id.action_cartFragment_to_orderFragment,
                null,
                navOptions
            )
        }
    }
}