package com.ardanu.challenge_ch4.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ardanu.challenge_ch4.ViewModelFactory
import com.ardanu.challenge_ch4.R
import com.ardanu.challenge_ch4.adapter.MenuAdapter
import com.ardanu.challenge_ch4.databinding.FragmentHomeBinding
import com.ardanu.challenge_ch4.model.MenuMakanan
import com.ardanu.challenge_ch4.viewmodel.HomeViewModel

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private lateinit var homeViewModel: HomeViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewModel()
        checkDataMenu()
    }

    private fun setupViewModel() {
        val factory = ViewModelFactory(requireActivity().application)
        homeViewModel = ViewModelProvider(this, factory)[HomeViewModel::class.java]
    }

    private fun checkDataMenu() {
        val menu = dataMenu()
        if (menu.isEmpty()) {
            binding.rvMenu.isVisible = false
            binding.layoutNoData.root.isVisible = true
        } else {
            binding.layoutNoData.root.isVisible = false
            binding.rvMenu.isVisible = true

            homeViewModel.getLayoutPref().observe(viewLifecycleOwner) {
                showRecyclerView(it)
                changeModeRecyclerView()
            }
        }
    }

    private fun changeModeRecyclerView() {
        homeViewModel.getLayoutPref().observe(viewLifecycleOwner) { mode ->
            binding.btnModeMenu.setOnClickListener {
                val newMode = !mode
                showRecyclerView(newMode)
                homeViewModel.saveLayoutPref(newMode)
            }
        }

    }

    private fun showRecyclerView(isGrid: Boolean) {
        val navController = findNavController()
        val menu = dataMenu()
        binding.rvMenu.adapter = MenuAdapter(menu, isGrid, onItemClick = {
            val bundle = Bundle()
            bundle.putParcelable("SELECTED_MENU", it)
            navController.navigate(R.id.action_homeFragment_to_detailMenuFragment, args = bundle)
        })
        if (isGrid) {
            binding.btnModeMenu.setImageResource(R.drawable.ic_grid)
            binding.rvMenu.layoutManager = GridLayoutManager(requireActivity(), 2)
        } else {
            binding.btnModeMenu.setImageResource(R.drawable.ic_list)
            binding.rvMenu.layoutManager = LinearLayoutManager(requireActivity())
        }

    }

    private fun dataMenu(): List<MenuMakanan> {
        // Inisialisasi data menu makanan
        val menuMakanan = mutableListOf<MenuMakanan>()
        menuMakanan.add(
            MenuMakanan(
                R.drawable.burger,
                "Burger",
                28000,
                "Menu cepat saji yang memiliki isian yang banyak dan mengenyangkan",
                "Jl. Mayor Oking Citeureup Gg. Prihatin No.RT03/03, Gn. Putri, Kec. Gn. Putri, Kabupaten Bogor, Jawa Barat 16961",
                "https://maps.app.goo.gl/8zLZfQP5pGMm2tEJ7"
            )
        )
        menuMakanan.add(
            MenuMakanan(
                R.drawable.ayamgoreng,
                "Ayam Goreng",
                20000,
                "Hidangan dari ayam dengan bumbu rempah-rempah yang khas dan digoreng hingga menjadi renyah",
                "Jl. Karang Kobar No.5, Karangkobar, Sokanegara, Kec. Purwokerto Tim., Kabupaten Banyumas, Jawa Tengah 53115",
                "https://maps.app.goo.gl/irNJZekVWDK2hNnw9"
            )
        )
        menuMakanan.add(
            MenuMakanan(
                R.drawable.ayampanggang,
                "Ayam Panggang",
                25000,
                "Hidangan dari ayam dengan lalu dipanggang atau dibakar hingga matang dan berwarna kecokelatan",
                "Jl. Jend. Gatot Subroto No. 68D, Purwanegara, Purwokerto Utara, Brubahan, Purwanegara, Banyumas, Kabupaten Banyumas, Jawa Tengah 53116",
                "https://maps.app.goo.gl/NTaLvgrN6hypf3Zg6"
            )
        )
        menuMakanan.add(
            MenuMakanan(
                R.drawable.dimsum,
                "Dim Sum",
                27000,
                "Variasi dari pangsit tradisional China yang disajikan dengan cara dikukus dan digoreng",
                "Jl.BSD Green Office Park Jl. BSD Grand Boulevard, Sampora, BSD, Kabupaten Tangerang, Banten 15345",
                "https://maps.app.goo.gl/h4wQKqaBuXzftGK77"
            )
        )
        for (i in 1..4) {
            menuMakanan.add(
                MenuMakanan(
                    R.drawable.burger,
                    "Burger $i",
                    28000,
                    "Menu cepat saji yang memiliki isian yang banyak dan mengenyangkan",
                    "Jl. Mayor Oking Citeureup Gg. Prihatin No.RT03/03, Gn. Putri, Kec. Gn. Putri, Kabupaten Bogor, Jawa Barat 16961",
                    "https://maps.app.goo.gl/8zLZfQP5pGMm2tEJ7"
                )
            )
            menuMakanan.add(
                MenuMakanan(
                    R.drawable.ayamgoreng,
                    "Ayam Goreng $i",
                    20000,
                    "Hidangan dari ayam dengan bumbu rempah-rempah yang khas dan digoreng hingga menjadi renyah",
                    "Jl. Karang Kobar No.5, Karangkobar, Sokanegara, Kec. Purwokerto Tim., Kabupaten Banyumas, Jawa Tengah 53115",
                    "https://maps.app.goo.gl/irNJZekVWDK2hNnw9"
                )
            )
            menuMakanan.add(
                MenuMakanan(
                    R.drawable.ayampanggang,
                    "Ayam Panggang $i",
                    25000,
                    "Hidangan dari ayam dengan lalu dipanggang atau dibakar hingga matang dan berwarna kecokelatan",
                    "Jl. Jend. Gatot Subroto No. 68D, Purwanegara, Purwokerto Utara, Brubahan, Purwanegara, Banyumas, Kabupaten Banyumas, Jawa Tengah 53116",
                    "https://maps.app.goo.gl/NTaLvgrN6hypf3Zg6"
                )
            )
            menuMakanan.add(
                MenuMakanan(
                    R.drawable.dimsum,
                    "Dim Sum $i",
                    27000,
                    "Variasi dari pangsit tradisional China yang disajikan dengan cara dikukus dan digoreng",
                    "Jl.BSD Green Office Park Jl. BSD Grand Boulevard, Sampora, BSD, Kabupaten Tangerang, Banten 15345",
                    "https://maps.app.goo.gl/h4wQKqaBuXzftGK77"
                )
            )
        }
        return menuMakanan
    }

}