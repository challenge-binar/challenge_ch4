package com.ardanu.challenge_ch4.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.ardanu.challenge_ch4.R
import com.ardanu.challenge_ch4.ViewModelFactory
import com.ardanu.challenge_ch4.databinding.FragmentDetailMenuBinding
import com.ardanu.challenge_ch4.model.MenuMakanan
import com.ardanu.challenge_ch4.viewmodel.DetailViewModel


class DetailMenuFragment : Fragment() {
    private lateinit var binding: FragmentDetailMenuBinding
    private lateinit var detailViewModel: DetailViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailMenuBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewModel()
        setDataToLayout()
        setEventUI()
        observeData()

    }

    private fun setupViewModel() {
        val factory = ViewModelFactory(requireActivity().application)
        detailViewModel = ViewModelProvider(this, factory)[DetailViewModel::class.java]
    }

    private fun setEventUI() {
        binding.ivBack.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.btnMin.setOnClickListener {
            detailViewModel.minItem()
        }
        binding.btnPlus.setOnClickListener {
            detailViewModel.addItem()
        }
    }

    private fun observeData() {
        detailViewModel.totalItem.observe(viewLifecycleOwner) {
            Log.e("Manakan", it.toString())
            binding.txtTotalItem.text = it.toString()
        }

        detailViewModel.totalPrice.observe(viewLifecycleOwner) {
            val addCartText = "Tambah Keranjang Rp.$it"
            binding.btnAddCart.text = addCartText
        }

        detailViewModel.allCarts.observe(viewLifecycleOwner) { listCart ->
            binding.btnAddCart.setOnClickListener {
                detailViewModel.addDataToCart(listCart)
                Toast.makeText(requireContext(), "Berhasil tambah ke keranjang", Toast.LENGTH_SHORT)
                    .show()
                val navOptions = NavOptions.Builder()
                    .setPopUpTo(R.id.homeFragment, true)
                    .build()
                findNavController().navigate(
                    R.id.action_detailMenuFragment_to_cartFragment,
                    null,
                    navOptions = navOptions
                )
            }
        }
    }

    @Suppress("DEPRECATION")
    private fun getData(): MenuMakanan {
        return arguments?.getParcelable("SELECTED_MENU")!!
    }

    private fun setDataToLayout() {
        val menu = getData()
        detailViewModel.getDataMenu(menu)

        val priceText = "Rp. ${menu.price}"
        val addCartText = "Tambah Keranjang Rp.${menu.price}"

        binding.ivImageDetail.setImageResource(menu.imageRes)
        binding.txtNameDetail.text = menu.name
        binding.txtDescriptionDetail.text = menu.menuDescription
        binding.txtPriceDetail.text = priceText
        binding.txtAddressDetail.text = menu.address
        binding.btnAddCart.text = addCartText

        binding.txtAddressDetail.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(menu.urlAddress))
            startActivity(intent)
        }
    }
}