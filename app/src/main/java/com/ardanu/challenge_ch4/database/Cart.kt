package com.ardanu.challenge_ch4.database

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize


@Entity(tableName = "cart")
@Parcelize
data class Cart(
    @PrimaryKey(autoGenerate = true)
    var id : Int = 0,
    var name: String,
    var imageRes: Int,
    var quantity: Int,
    var totalPrice: Int,
    val basePrice: Int,
    var notes:String? = ""
): Parcelable
