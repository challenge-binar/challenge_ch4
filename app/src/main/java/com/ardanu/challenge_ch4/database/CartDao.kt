package com.ardanu.challenge_ch4.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface CartDao {
    @Insert
    fun insert(cart: Cart)

    @Update
    fun update(cart: Cart)

    @Query("DELETE FROM cart where id = :id")
    fun deleteCart(id: Int)

    @Query("DELETE FROM cart")
    fun deleteAllCart()

    @Query("SELECT * FROM cart ORDER BY id DESC")
    fun getAllCart(): LiveData<List<Cart>>

}