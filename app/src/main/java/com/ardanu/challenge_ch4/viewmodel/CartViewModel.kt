package com.ardanu.challenge_ch4.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.ardanu.challenge_ch4.database.Cart
import com.ardanu.challenge_ch4.database.CartDao
import com.ardanu.challenge_ch4.database.RestaurantDatabase

class CartViewModel(application: Application) : ViewModel() {

    private val _database = RestaurantDatabase.getInstance(application)
    private val _carts: CartDao = _database.cartDao

    val allCarts: LiveData<List<Cart>> = _carts.getAllCart()

    fun deleteItemCart(cartId: Int) {
        _carts.deleteCart(cartId)
    }

    fun deleteAllCart(){
        _carts.deleteAllCart()
    }

    fun updateNotes(note: String,cart: Cart){
        cart.notes = note
        updateItemCart(cart)
    }

    private fun updateItemCart(cart: Cart) {
        _carts.update(cart)
    }

    fun addItem(cart: Cart) {

        val newTotal = cart.quantity + 1
        cart.quantity = newTotal
        cart.totalPrice = cart.basePrice * newTotal
        updateItemCart(cart)

    }

    fun minItem(cart: Cart) {
        if (cart.quantity > 1){
            val newTotal = cart.quantity - 1
            cart.quantity = newTotal
            cart.totalPrice = cart.basePrice * newTotal
            updateItemCart(cart)
        }
    }
}