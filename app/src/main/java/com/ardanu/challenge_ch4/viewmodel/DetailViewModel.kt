package com.ardanu.challenge_ch4.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ardanu.challenge_ch4.database.Cart
import com.ardanu.challenge_ch4.database.CartDao
import com.ardanu.challenge_ch4.database.RestaurantDatabase
import com.ardanu.challenge_ch4.model.MenuMakanan

class DetailViewModel(application: Application) : ViewModel() {

    private val _database = RestaurantDatabase.getInstance(application)
    private val _carts: CartDao = _database.cartDao

    private val _totalItem: MutableLiveData<Int> = MutableLiveData(1)
    val totalItem: LiveData<Int> get() = _totalItem

    private val _totalPrice: MutableLiveData<Int> = MutableLiveData()
    val totalPrice: LiveData<Int> get() = _totalPrice

    private val _selectedMenu = MutableLiveData<MenuMakanan>()

    val allCarts: LiveData<List<Cart>> = _carts.getAllCart()
    fun addDataToCart(listCart: List<Cart>) {
        val selectedMenu = _selectedMenu.value
        Log.e("CEK_DATABASE",listCart.size.toString())

        selectedMenu?.let { menu ->
            val itemCart = totalItem.value?.let { qty ->
                totalPrice.value?.let { price ->
                    Cart(
                        name = menu.name,
                        imageRes = menu.imageRes,
                        quantity = qty,
                        totalPrice = price,
                        basePrice = menu.price
                    )
                }
            }

            itemCart?.let { item ->
                val oldData = listCart.find { it.name == item.name }
                if (oldData != null){
                    val newData = Cart(
                        id = oldData.id,
                        name = oldData.name,
                        imageRes = oldData.imageRes,
                        quantity = oldData.quantity + item.quantity,
                        totalPrice = oldData.totalPrice + item.totalPrice,
                        basePrice = oldData.basePrice
                    )
                    _carts.update(newData)
                }else{
                   _carts.insert(itemCart)
                }
            }
        }

    }


    fun getDataMenu(menu: MenuMakanan) {
        _selectedMenu.value = menu
        _totalPrice.value = menu.price
    }

    fun addItem() {
        _totalItem.value?.let {
            _totalItem.value = it + 1
            updatePrice()
        }
    }

    fun minItem() {
        _totalItem.value?.let {
            if (it > 1) {
                _totalItem.value = it - 1
                updatePrice()

            }
        }
    }

    private fun updatePrice() {
        val totalItem = _totalItem.value ?: 1
        val selectedMenu = _selectedMenu.value
        if (selectedMenu != null) {
            val totalPrice = selectedMenu.price * totalItem
            _totalPrice.value = totalPrice
        }
    }


}