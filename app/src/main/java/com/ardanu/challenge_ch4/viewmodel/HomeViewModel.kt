package com.ardanu.challenge_ch4.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.ardanu.challenge_ch4.LayoutDataStore
import kotlinx.coroutines.launch

class HomeViewModel(application: Application): ViewModel() {

    private val pref: LayoutDataStore = LayoutDataStore(application)
    fun saveLayoutPref(value: Boolean) {
        viewModelScope.launch {
            pref.setLayoutPref(value)
        }
    }

    fun getLayoutPref(): LiveData<Boolean> {
        return pref.getLayoutPref().asLiveData()
    }

}