package com.ardanu.challenge_ch4.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ardanu.challenge_ch4.databinding.GridMenuItemBinding
import com.ardanu.challenge_ch4.databinding.ListMenuItemBinding
import com.ardanu.challenge_ch4.model.MenuMakanan

class MenuAdapter(
    private val menuList: List<MenuMakanan>,
    private val isGrid: Boolean,
    private var onItemClick: ((MenuMakanan) -> Unit)? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (isGrid) {
            val binding =
                GridMenuItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            GridMenuHolder(binding)
        } else {
            val binding =
                ListMenuItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            LinearMenuHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = menuList[position]

        if (isGrid) {
            val gridHolder = holder as GridMenuHolder
            gridHolder.onBind(item, onItemClick)
        } else {
            val linearHolder = holder as LinearMenuHolder
            linearHolder.onBind(item, onItemClick)
        }

    }

    override fun getItemCount(): Int = menuList.size

    class GridMenuHolder(private val binding: GridMenuItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(data: MenuMakanan, onItemClick: ((MenuMakanan) -> Unit)? = null) {
            val priceText = "Rp. ${data.price}"

            binding.imgMenu.setImageResource(data.imageRes)
            binding.nameMenu.text = data.name
            binding.priceMenu.text = priceText
            binding.root.setOnClickListener {
                onItemClick?.invoke(data)

            }
        }

    }

    class LinearMenuHolder(private val binding: ListMenuItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(data: MenuMakanan, onItemClick: ((MenuMakanan) -> Unit)? = null) {
            val priceText = "Rp. ${data.price}"

            binding.imgMenuList.setImageResource(data.imageRes)
            binding.nameMenuList.text = data.name
            binding.priceMenuList.text = priceText
            binding.root.setOnClickListener {
                onItemClick?.invoke(data)
            }
        }

    }
}

