package com.ardanu.challenge_ch4.adapter

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doAfterTextChanged
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.ardanu.challenge_ch4.database.Cart
import com.ardanu.challenge_ch4.databinding.CartItemBinding
import com.ardanu.challenge_ch4.viewmodel.CartViewModel

class CartAdapter(
    private val cartViewModel: CartViewModel,
    private val isOrder: Boolean
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var cartList: List<Cart> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged")
    fun setList(cartList: List<Cart>) {
        this.cartList = cartList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = CartItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CartViewHolder(binding)
    }

    override fun getItemCount(): Int = cartList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = cartList[position]

        val cartHolder = holder as CartViewHolder
        cartHolder.onBind(item, cartViewModel, isOrder)
    }

    class CartViewHolder(private val binding: CartItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(data: Cart, viewModel: CartViewModel, isOrder: Boolean) {

            val qtyTextOrder = "${data.quantity}x"
            val menuPriceText = "Rp. ${data.basePrice}"

            if (isOrder) {
                binding.btnDeleteMenu.visibility = View.INVISIBLE
                binding.btnPlusMenu.visibility = View.GONE
                binding.btnMinMenu.visibility = View.GONE
                binding.imgMenuCart.setImageResource(data.imageRes)
                binding.nameMenuCart.text = data.name
                data.notes?.let { setEditText(it) }
                binding.noteMenuCart.isEnabled = false
                binding.totalItemCart.text = qtyTextOrder
                binding.priceMenuCart.text = menuPriceText

            } else {
                binding.imgMenuCart.setImageResource(data.imageRes)
                binding.nameMenuCart.text = data.name
                binding.totalItemCart.text = data.quantity.toString()
                binding.priceMenuCart.text = menuPriceText
                data.notes?.let { setEditText(it) }

                binding.noteMenuCart.setOnKeyListener { v, keyCode, event ->
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        // Perform action on key press
                        Log.e("AfterChange",binding.noteMenuCart.text.toString())
                        val newNotes = binding.noteMenuCart.text.toString()
                        viewModel.updateNotes(newNotes,data)
                        binding.noteMenuCart.clearFocus()
                         return@setOnKeyListener true
                    }
                    return@setOnKeyListener false
                }

                binding.btnDeleteMenu.setOnClickListener {
                    viewModel.deleteItemCart(data.id)
                }

                binding.btnPlusMenu.setOnClickListener {
                    viewModel.addItem(data)
                    binding.totalItemCart.text = data.quantity.toString()
                    binding.priceMenuCart.text
                }
                binding.btnMinMenu.setOnClickListener {
                    viewModel.minItem(data)
                    binding.totalItemCart.text = data.quantity.toString()

                }
            }
        }

        private fun setEditText(notes: String){
            if (notes != null){
                binding.noteMenuCart.setText(notes)
            }else{
                binding.noteMenuCart.setText("")
            }
        }

    }
}