package com.ardanu.challenge_ch4.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MenuMakanan(
    val imageRes: Int,
    val name: String,
    val price: Int,
    val menuDescription: String,
    val address: String,
    val urlAddress: String
) : Parcelable
